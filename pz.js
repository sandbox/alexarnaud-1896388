// Wrapper for jQuery
(function ($) {

  // Redefine this core function from misc/autocomplete.js because
  // we get an error when submitting the form while autocomplete
  // AJAX request is processing. Here we replace message alert with
  // a console.log.
  Drupal.ACDB.prototype.search = function (searchString) {
    var db = this;
    this.searchString = searchString;

    // See if this string needs to be searched for anyway.
    searchString = searchString.replace(/^\s+|\s+$/, '');
    if (searchString.length <= 0 ||
      searchString.charAt(searchString.length - 1) == ',') {
      return;
    }

    // See if this key has been searched for before.
    if (this.cache[searchString]) {
      return this.owner.found(this.cache[searchString]);
    }

    // Initiate delayed search.
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(function () {
      db.owner.setStatus('begin');

      // Ajax GET request for autocompletion. We use Drupal.encodePath instead of
      // encodeURIComponent to allow autocomplete search terms to contain slashes.
      $.ajax({
        type: 'GET',
        url: db.uri + '/' + Drupal.encodePath(searchString),
        dataType: 'json',
        success: function (matches) {
          if (typeof matches.status == 'undefined' || matches.status != 0) {
            db.cache[searchString] = matches;
            // Verify if these are still the matches the user wants to see.
            if (db.searchString == searchString) {
              db.owner.found(matches);
            }
            db.owner.setStatus('found');
          }
        },
        error: function (xmlhttp) {
          if (window.console) {
            console.log(Drupal.ajaxError(xmlhttp, db.uri));
          }
        }
      });
    }, this.delay);
  };
})(jQuery);

var pz = (function($) {
  var pz = {},
    expanded = Object(),
    sort,
    session,
    records,
    keepaliveid = -1;

  pz.start = function() {
    var session = Drupal.settings.pz.session;

    if (session) {
      $('#pz-info').html('<span><b>' + Drupal.t('Query', {}, {'context': 'libraries'}) + ': </b><span id="pz-info-query"></span></span>');
      $('#pz-info-query').text(Drupal.settings.pz.query_desc);
      $('.pz-search-info').html(Drupal.t('Searching') + '...');

      if (Drupal.settings.pz.hide_form) {
        // Hide form and add a link to redisplay it.
        var $show_link = $('<a href="#">' + Drupal.t('Return to search') + '</a>');
        $show_link.click(function() {
          $('form.pz-search').show();
          $(this).remove();
          return false;
        });
        $('form.pz-search').hide().after($show_link);
      }

      // Session keep-alive
      if (keepaliveid != -1) {
        clearInterval(keepaliveid);
      }
      keepaliveid = setInterval(function() {
        pz.keepalive(session);
      }, 50000);

      sort = $('#pz-sort').val();
      start = 0;
      offset = 20;
      pz.showResults(session, sort, start, offset);
    }
  }

  // Called when user click on 'Reset' button.
  pz.onFormReset = function () {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    pz.reset();
    return false;
  }

  pz.showResults = function(session, sort, start, offset) {
    pz.stats(session).then(function(stats) {
      pz.show(session, sort, start, offset, stats).then(function() {
        pz.updateFacets(session);
        if (stats['progress'] < 1) {
          setTimeout(function() {
            pz.showResults(session, sort, start, offset);
          }, 500);
        }
      });
    });
  }

  pz.reset = function () {
    $('.pz-query-field').each(function(index) {
      if (index > 2) {
        $(this).remove();
      }
    });
    var form = $('#pz-search-page');
    form.find('input[type="text"]').val('');
    form.find('input[type="checkbox"]').removeAttr('checked');
    form.find('select').val('');
    $('.pz-info, .pz-search-info').empty();
    $('.pz-results').empty();
  }

  // Session keep-alive
  pz.keepalive = function (session, query, targets) {
    var url = '/pz/keepalive/' + session;
    $.ajax({
      url: url
    });
  }

  pz.show = function (session, sort, start, offset, stats) {
    var url = '/pz/show/' + session + '/' + sort + '/' + start + '/' + offset;
    return $.ajax({
      type: 'GET',
      url: url,
      success: function(data, textStatus, jqXHR) {
        $('.pz-results').html(data['html']);
        var throbber = '<img src="/misc/throbber-active.gif">';
        status_string = stats['progress'] == 1 ? Drupal.t('Search done') + '. ' : throbber + ' ' + Drupal.t('Searching') + '... ';
        records = data['merged'][0];

        if (records > 0) {
          pz.buildPagination(start, offset, records, session, sort);
          var trecords = Drupal.formatPlural(data['merged'][0], '1 record', '@count records', {}, {'context': 'libraries'});

          var end = Math.min(start + offset, records);
          $('.pz-search-info').html(status_string + (start + 1) + '-' + end + ' ' + Drupal.t('of') + ' ' + trecords);

          // Let's hide all the definitions.
          $('.pz-results dd').hide();

          // But not the one the user had already expanded.
          for (var i in expanded) {
            if (i && expanded[i] == 1) {
              $('#' + i).show();
            }
          }

          // And show them on click
          $('.pz-results dt').click(function() {

            // We show it
            $(this).next().toggle();

            // Get the id
            var id = $(this).next().attr('id');

            // And mark it as to be displayed, or not.
            if (expanded[id] != 1) {
              expanded[id] = 1;
            } else {
              expanded[id] = 0;
            }
          });

          // This is for author search-on-click
          $('a.pz-principal-author').bind('click', pz.authorsearch);
          $('a.pz-author').bind('click', pz.authorsearch);
          $('a.pz-subject').bind('click', pz.subjectsearch);
          $('.pz-prev').bind('click', pz.prev);
          $('.pz-next').bind('click', pz.next);
        } else {
          var no_results_string = (stats.progress == 1)
            ? Drupal.t('No results found')
            : Drupal.t('No results yet');
          $('.pz-search-info').html(status_string + no_results_string);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        $('.pz-results').html('<span>No result found.</span>')
      }
    });
  }

  pz.termlist = function(session, name, success_callback) {
    var url = '/pz/termlist?session=' + session + '&name=' + name;
    $.ajax({
      'url': url,
      'success': success_callback,
    });
  }

  pz.appendLimit = function(facet, value, remove) {
    var enabled = {};
    jQuery.extend(true, enabled, Drupal.settings.pz.query.limit);

    if (remove == false) {
      if (facet in enabled) {
        enabled[facet].push(value);
      }
      else {
        enabled[facet] = [value];
      }
    }

    var limit = '';
    for (facetname in enabled) {
      for (var i in enabled[facetname]) {
        if (remove == true && facetname == facet && enabled[facetname][i] == value) continue;
        limit += '&limit[' + facetname + ']';
        limit += '[' + i + ']=' + enabled[facetname][i];
      }
    }
    return limit;
  }

  pz.updateFacets = function(session) {
    $('.block-pz').each(function() {
      var $block = $(this);
      var id = this.id;
      var facet = id.replace('block-pz-facet-', '');
      var facets = {};
      jQuery.extend(true, facets, Drupal.settings.pz.query.limit);
      facets = (facet in facets) ? facets[facet] : [];
      var termlist = pz.termlist(session, facet, function(data) {
        var id = 'block-pz-facet-' + facet;
        var content = $('<ul></ul>');
        var terms = [];
        if (data.list.term && data.list.term.length) {
          terms = data.list.term;
        } else if (data.list.term) {
          terms = [ data.list.term ];
        }
        for (var i in facets) {
          var href = window.location.pathname + '?' + Drupal.settings.pz.query_url;
          href += pz.appendLimit(facet, facets[i], true);
          var facet_item = $('<li></li>');
          var facet_link = $('<a>' + facets[i] + '</a>')
            .addClass('pz-facet-link')
            .addClass('pz-facet-' + facet + '-link')
            .addClass('pz-facet-link-enabled')
            .addClass('pz-facet-' + facet + '-link-enabled')
            .attr('title', Drupal.t('Remove facet'))
            .attr('href', href);
          facet_item.append(facet_link);
          content.append(facet_item);
        }

        for (i in terms) {
          var term = terms[i];
          if ($.inArray(term.name, facets) == -1) {
            var href = window.location.pathname + '?' + Drupal.settings.pz.query_url;
            href += pz.appendLimit(facet, term.name, false);
            var facet_item = $('<li></li>');
            var facet_link = $('<a>' + term.name + '</a>')
              .addClass('pz-facet-link')
              .addClass('pz-facet-' + facet + '-link')
              .attr('href', href);
            facet_item.append(facet_link);
            facet_item.append(' (' + term.frequency + ')');
            content.append(facet_item);
          }
        }

        // Hide some facets
        var facetsToShow = Drupal.settings.pz.number_facets_shown;
        var nbFacets = content.children('li').length;
        if (facetsToShow && nbFacets > parseInt(facetsToShow)) {
          var showMoreTitle = Drupal.t(Drupal.settings.pz.show_more_title);
          var showFewerTitle = Drupal.t(Drupal.settings.pz.show_fewer_title);
          var hiddenElements = content.children('li:gt(' + (parseInt(facetsToShow) -1) + ')').hide();
          content.append(
            $('<li class="pz-facets-toggler"><a href="">' + showMoreTitle + '</a></li>').toggle(
              function() {
                hiddenElements.show();
                $(this).children('a').text(showFewerTitle);
              },
              function() {
                hiddenElements.hide();
                $(this).children('a').text(showMoreTitle);
              }
          ));
        }

        $block.find('.content').html(content);
        if (content.find('li').length > 0) {
          $block.show();
          $block.parents()
            .filter(function(){ return $(this).css('visibility') == 'hidden' })
            .css('visibility', '');
        } else {
          $block.hide();
        }
      });
    });
  }

  pz.stats = function (session) {
    var url = '/pz/stats/' + session;
    return $.ajax({
      type: 'GET',
      url: url,
    });
  }

  pz.buildPagination = function (start, offset, records, session, sort) {
    var first, previous, next, last;

    var page_count = Math.ceil((records - 1) / offset);

    // Are we at the last page?
    if (start + offset <= records) {
      next = $('<a>')
        .attr('href', '#')
        .attr('title', Drupal.t('Show next results'))
        .addClass('pz-next')
        .html(Drupal.t('Next') + ' >')
        .click(function() {
          pz.showResults(session, sort, start + offset, offset);
          return false;
        });
      last = $('<a>')
        .attr('href', '#')
        .attr('title', Drupal.t('Show last results'))
        .addClass('pz-last')
        .html(Drupal.t('Last') + ' »')
        .click(function() {
          pz.showResults(session, sort, (page_count - 1) * offset, offset);
          return false;
        });
    } else {
      next = $('<span>').html(Drupal.t('Next') + ' >');
      last = $('<span>').html(Drupal.t('Last') + ' »');
    }

    // Are we at the first page?
    if (start > 0) {
      previous = $('<a>')
        .attr('href', '#')
        .attr('title', Drupal.t('Show previous results'))
        .addClass('pz-prev')
        .html('< ' + Drupal.t('Previous'))
        .click(function() {
          pz.showResults(session, sort, start - offset, offset);
          return false;
        });
      first = $('<a>')
        .attr('href', '#')
        .attr('title', Drupal.t('Show first results'))
        .addClass('pz-first')
        .html('« ' + Drupal.t('First'))
        .click(function() {
          pz.showResults(session, sort, 0, offset);
          return false;
        });
    } else {
      previous = $('<span>').html('< ' + Drupal.t('Previous'));
      first = $('<span>').html('« ' + Drupal.t('First'));
    }

    // Page numbers
    var current_page = (start + offset) / offset;
    var pagination_start = Math.max(Math.min(current_page - 4, page_count - 8), 1);
    var pagination_end = Math.min(pagination_start + 8, page_count);
    var pages = new Array();

    // We need this function factory to make a copy of the 'i' parameter
    function pageClickFunctionFactory(i) {
      return function() {
        pz.showResults(session, sort, offset * (i - 1), offset);
        return false;
      }
    }
    for (i = pagination_start; i <= pagination_end; i++) {
      var page;

      // Are we on the current page?
      if (current_page != i) {
        var j = i;
        page = $('<a>')
          .attr('href', '#')
          .html(i)
          .click(pageClickFunctionFactory(i));
      } else {
        page = $('<span>').html(i);
      }

      pages.push(page);
    }

    var $navigation = $('.pz-navigation');
    $navigation.html('')
      .append(first)
      .append(previous);

    for (i in pages) {
      $navigation.append(pages[i]);
    }

    $navigation
      .append(next)
      .append(last);
  }

  return pz;
})(jQuery);

(function($) {
  $(document).ready(function () {
    // Change '+' to be a button instead of a submit.
    // This avoid the button to be clicked when user hits Enter in form fields.
    $('#edit-new-field').get(0).type = 'button';

    $('.block-pz').hide();

    $('#pz-reset').bind('click', pz.onFormReset);

    $('form.pz-search')
      .bind('submit', function() {
        $(this)
          .find('[name="form_build_id"],[name="form_token"],[name="form_id"]')
          .attr('disabled', 'disabled');
      })
      .parent()
        .append('<div id="pz-info" class="pz-info"></div>')
        .append('<div id="pz-search-info" class="pz-search-info"></div>')
        .append('<div id="pz-results" class="pz-results"></div>');

    pz.start();
  });
})(jQuery);
