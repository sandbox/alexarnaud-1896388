<?php

/**
 * @file
 * Template file used for each search result.
 */

/*
 * Available variables:
 *   $hit: The search result to be displayed.
 */

?>

<?php
  $id_attr = 'md-id';
  $title_attr = 'md-title';
  $principal_author_attr = 'md-principal-author';
  $author_attr = 'md-author';
  $date_attr = 'md-date';
  $source_attr = 'md-electronic-url';
  $desc_attr = 'md-description';
  $subject_attr = 'md-subject';
  $pubname_attr = 'md-publication-name';
  $pubdate_attr = 'md-publication-date';

  $title = (string) $hit->{$title_attr};
  $principal_author = (string) $hit->{$principal_author_attr};
  $principal_author_href = "/pz/search_page?fields[0][index]=au&fields[0][value]=" . rawurlencode($principal_author) . "&sort=relevance:0";
  $authors = array();
  foreach ($hit->{$author_attr} as $a):
    $authors[] = (string) $a;
  endforeach;
  $subjects = array();
  foreach ($hit->{$subject_attr} as $s):
    $subjects[] = (string) $s;
  endforeach;
  $publication_name = (string) $hit->{$pubname_attr};
  $publication_date = (string) $hit->{$pubdate_attr};
  if (isset($hit->{$date_attr})):
    $date = (string) $hit->{$date_attr};
  endif;

  $locations = array();
  $sources = array();
  foreach ($hit->location as $loc):
    $locations[] = (string) $loc['name'];
    $sources[] = $loc->{$source_attr};
    $id = $loc->{$id_attr};
  endforeach;
  $locations = array_unique($locations);

  // If we don't have an id, we create one.
  if (!$id):
    $id = md5(implode($sources) . $title);
  endif;

  // Description can be in an array, so we concat it if needed.
  $descarray = array();
  foreach ($hit->{$desc_attr} as $value):
    $descarray[] = strval($value);
  endforeach;
  $desc = implode("<br />", $descarray);
?>

<div class="pz-one-result">
  <dt>
    <a href="#" title="<?php echo htmlspecialchars(t('Expand result')); ?>" onclick="return false;"><?php echo $title; ?></a>
    <?php if ($principal_author): ?>
      <?php echo t('by'); ?>
      <a class="pz-principal-author" title="<?php echo htmlspecialchars(t("Start a new search on author")); ?>" href="<?php echo $principal_author_href; ?>"><?php echo $principal_author ?></a>
    <?php endif; ?>
    <?php if ($publication_date): ?>
    , <?php echo $publication_date; ?>
    <?php endif; ?>
  </dt>
  <dd id="<?php echo $id ?>">
    <?php if ($desc): ?>
      <div class="pz-md-description"><?php echo $desc ?></div>
    <?php endif; ?>
    <?php if ($authors): ?>
      <div class="pz-md-author">
        <?php echo t('Authors') . ': ' ?>
        <?php $idx = 0; ?>
        <?php
          foreach ($authors as $a):
            $href = "/pz/search_page?fields[0][index]=au&fields[0][value]=" . rawurlencode($a) . "&sort=relevance:0";
            if ($idx > 0): echo ', '; endif;
            echo '<a class="pz-author" title="' . htmlspecialchars(t("Start a new search on author")) . '" href="' . $href . '">' . $a . '</a>';
            $idx++;
          endforeach;
        ?>
      </div>
    <?php endif; ?>
    <?php if ($subjects): ?>
      <div class="pz-md-subject">
        <?php echo t('Subjects') . ': ' ?>
        <?php $idx = 0; ?>
        <?php
          foreach ($subjects as $a):
            $href = "/pz/search_page?fields[0][index]=su&fields[0][value]=" . rawurlencode($a) . "&sort=relevance:0";
            if ($idx > 0): echo '; '; endif;
            echo '<a class="pz-subject" title="' . htmlspecialchars(t('Start a new search on subject')) . '" href="' . $href . '">' . $a . '</a>';
            $idx++;
          endforeach;
        ?>
      </div>
    <?php endif; ?>
    <?php if ($date): ?>
      <div class="pz-md-date"><?php echo $date ?></div>
    <?php endif; ?>
    <?php if ($publication_name || $publication_date): ?>
      <div class="pz-publication">
        <?php
          echo t('Publication') . ": $publication_name";
          if ($publication_date):
            echo ", $publication_date";
          endif;
        ?>
      </div>
    <?php endif; ?>
    <?php if ($locations): ?>
      <div><?php echo t('Found at') . ': ' . implode(', ', $locations) ?></div>
    <?php endif; ?>
    <?php foreach ($sources as $source): ?>
      <a href="<?php echo $source ?>"><?php echo $source ?></a>
    <?php endforeach; ?>

    <?php /* Useful for debug, add trailing slash to uncomment *
      echo '<pre>' . var_export($hit, TRUE) . '</pre>';
    /**/ ?>
  </dd>
</div>
