<?php

/**
 * @file
 * Functions for manipulating limits configuration in DB.
 */

/**
 * Add a new limit configuration.
 */
function pz_limit_add($limit) {
  db_insert('pz_limit')
    ->fields($limit)
    ->execute();
}

/**
 * Update a limit configuration.
 */
function pz_limit_update($limit) {
  db_update('pz_limit')
    ->fields($limit)
    ->condition('name', $limit['name'])
    ->execute();
}

/**
 * Get all limit configurations.
 */
function pz_limit_get_all() {
  $result = db_select('pz_limit', 'pl')
    ->fields('pl')
    ->orderBy('weight')
    ->execute();

  $limits = array();
  while ($entry = $result->fetchAssoc()) {
    $limits[] = $entry;
  }

  return $limits;
}

/**
 * Get a limit configuration by its name.
 */
function pz_limit_get($name) {
  $result = db_select('pz_limit', 'pl')
    ->fields('pl')
    ->condition('name', $name)
    ->execute();

  return $result->fetchAssoc();
}

/**
 * Delete a limit configuration.
 */
function pz_limit_delete($name) {
  db_delete('pz_limit')
    ->condition('name', $name)
    ->execute();
}
