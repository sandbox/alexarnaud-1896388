<?php

/**
 * @file
 * PZ administration functions
 */

/**
 * Main configuration form.
 */
function pz_config_form($form, $form_state) {
  $pz_base_url = variable_get('pz_base_url', '');
  $form['pz_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Base URL'),
    '#default_value' => $pz_base_url,
  );

  $pz_hide_form = variable_get('pz_hide_form', NULL);
  $form['pz_hide_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide form'),
    '#description' => t('Hide search form when displaying results'),
    '#default_value' => $pz_hide_form,
  );

  $pz_autocomplete = variable_get('pz_autocomplete', FALSE);
  $form['pz_autocomplete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable autocompletion'),
    '#description' => t('Enable autocompletion on text fields'),
    '#default_value' => $pz_autocomplete,
  );

  $pz_limits_fieldset = variable_get('pz_limits_fieldset', FALSE);
  $form['pz_limits_fieldset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show limits in a collapsible fieldset'),
    '#default_value' => $pz_limits_fieldset,
  );

  $pz_default_field_count = variable_get('pz_default_field_count', 3);
  $form['pz_default_field_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of text fields'),
    '#description' => t('Default number of text fields displayed in the form'),
    '#default_value' => $pz_default_field_count,
  );

  $pz_date_from_label = variable_get('pz_date_from_label', 'Date (from)');
  $form['pz_date_from_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for field: Date (from)'),
    '#description' => t('It should be an untranslated string'),
    '#default_value' => $pz_date_from_label,
  );

  $pz_date_to_label = variable_get('pz_date_to_label', 'Date (to)');
  $form['pz_date_to_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for field: Date (to)'),
    '#description' => t('It should be an untranslated string'),
    '#default_value' => $pz_date_to_label,
  );

  $auto_configure_description
    = t("Click the button below to configure PZ module automatically.") . ' ' .
      t("It will ask directly to PazPar2 for its internal configuration and fill the fields below accordingly.");

  $form['auto_confirm_description'] = array(
    '#markup' => $auto_configure_description,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $form['auto_configure'] = array(
    '#type' => 'button',
    '#value' => t('Auto-configuration'),
    '#name' => 'auto-configure',
    '#ajax' => array(
      'callback' => 'pz_config_form_auto_configure_callback',
      'wrapper' => 'pz-config',
      'method' => 'replace',
    ),
    '#attributes' => array(
      'title' => t('Ask PazPar2 for its configuration to automatically fill fields below'),
    ),
  );

  $button = isset($form_state['clicked_button']) ?
    $form_state['clicked_button'] : NULL;
  $auto_configure = (isset($button) && $button['#name'] == 'auto-configure');
  if ($auto_configure) {
    $base_url = $form_state['values']['pz_base_url'];
    $url = "$base_url/?command=info";
    $info = simplexml_load_file($url);

    if ($info) {
      // Assert 'info' returns wanted data.
      if (isset($info->services->service->databases)) {
        // Build the targets configuration string.
        $targets = pz_config_form_info_targets($info);
        if (count($targets)) {
          $targets_string = implode(',', array_map(function ($target) {
            return $target['id'] . '|' . $target['name'];
          }, $targets));
        }

        // Build the facets configuration string.
        $facets = pz_config_form_info_facets($info);
        if (count($facets)) {
          $facets_string = implode(',', $facets);
        }
      }
      else {
        $message = t("PazPar2 is not returning the informations required for auto configuration. Please consider installing the latest version of PazPar2");
        drupal_set_message($message, 'error');
      }
    }
    else {
      $message = t('A problem occured while retrieving configuration. Are you sure the base URL is correct?');
      drupal_set_message($message, 'error');
    }

  }

  $form['config'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'pz-config'),
  );

  $pz_targets = variable_get('pz_targets', '');
  $form['config']['pz_targets'] = array(
    '#type' => 'textfield',
    '#title' => t('Targets'),
    '#description' => t("A comma-separated list of PazPar2 target ids. You can specify a display name by appending a pipe ('|') to the id, followed by the display name. Example: \"target_id1|Target 1,target_id2|Target 2"),
    '#default_value' => $pz_targets,
    '#maxlength' => NULL,
  );

  if (isset($targets_string)) {
    $form['config']['pz_targets']['#value'] = $targets_string;
  }

  $pz_facets = variable_get('pz_facets', '');
  $form['config']['pz_facets'] = array(
    '#type' => 'textfield',
    '#title' => t('Facets'),
    '#description' => t("A comma-separated list of fields on which to enable facetting. Each field will create a block that have to be placed in a theme region."),
    '#default_value' => $pz_facets,
    '#maxlength' => NULL,
  );

  if (isset($facets_string)) {
    $form['config']['pz_facets']['#value'] = $facets_string;
  }

  $form['pz_number_facets_shown'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of facets shown'),
    '#description' => t('Type here the max number of facets to show to the user. If 0 or empty, all facets are shown.'),
    '#size' => 8,
    '#default_value' => variable_get('pz_number_facets_shown', ''),
  );

  $form['pz_show_more_title'] = array(
    '#type' => 'textfield',
    '#title' => t('"Show more" title'),
    '#description' => t('Title of the link allowing to show all facets'),
    '#size' => 25,
    '#default_value' => variable_get('pz_show_more_title', 'Show more'),
  );

  $form['pz_show_fewer_title'] = array(
    '#type' => 'textfield',
    '#title' => t('"Show fewer" title'),
    '#description' => t('Title of the link allowing to show fewer facets'),
    '#size' => 25,
    '#default_value' => variable_get('pz_show_fewer_title', 'Fewer choices'),
  );

  $form['config']['pz_indexes'] = array(
    '#type' => 'textfield',
    '#title' => t('Searchable indexes'),
    '#description' => t("A comma-separated list of index:label. i.e 'ti:Title,su:Subject,au:Author'. Label is translatable"),
    '#default_value' => variable_get('pz_indexes', 'ti:Title,su:Subject,au:Author'),
    '#maxlength' => NULL,
  );

  $form['default_sort_method'] = array(
    '#type' => 'select',
    '#title' => t('Default sort method'),
    '#options' => pz_sort_options_list(),
    '#default_value' => variable_get('default_sort_method', 'relevance:0'),
    '#description' => t('Choose the sort method to be selected by default in search form.'),
  );

  return system_settings_form($form);
}

/**
 * Callback for 'auto-configure' button.
 */
function pz_config_form_auto_configure_callback($form, $form_state) {
  return $form['config'];
}

/**
 * Retrieve all targets.
 */
function pz_config_form_info_targets($info) {
  $targets = array();

  foreach ($info->services->service as $service) {
    if (isset($service->databases)) {
      foreach ($service->databases->database as $database) {
        $id = (string) $database['id'];
        foreach ($database->setting as $setting) {
          if ($setting['name'] == 'pz:name') {
            $name = (string) $setting['value'];
            break;
          }
        }
        $targets[] = array(
          'id' => $id,
          'name' => $name,
        );
      }
    }
  }

  return $targets;
}

/**
 * Retrieve all fields that can be faceted.
 */
function pz_config_form_info_facets($info) {
  $facets = array();

  $termlist = array();
  $limitmap = array();
  foreach ($info->services->service as $service) {
    if (isset($service->metadata)) {
      foreach ($service->metadata as $md) {
        if (isset($md['termlist']) && $md['termlist'] == 'yes') {
          $name = (string) $md['name'];
          $termlist[] = $name;
        }
      }
    }
    if (isset($service->databases)) {
      foreach ($service->databases->database as $database) {
        foreach ($database->setting as $setting) {
          if (substr($setting['name'], 0, 12) == 'pz:limitmap:') {
            $name = substr($setting['name'], 12);
            $limitmap[] = $name;
          }
        }
      }
    }
  }

  // Keep fields that have termlist="yes" and a pz:limitmap setting.
  $facets = array_intersect($termlist, $limitmap);

  return $facets;
}

/**
 * Display all limit configurations in a table.
 */
function pz_config_limits($form) {
  module_load_include('inc', 'pz', 'pz.limit');

  $form['#tree'] = TRUE;

  $limits = pz_limit_get_all();
  foreach ($limits as $limit) {
    $name = $limit['name'];
    $form['limits'][$name]['name'] = array(
      '#markup' => check_plain($limit['name']),
    );
    $form['limits'][$name]['label'] = array(
      '#markup' => check_plain($limit['label']),
    );
    $form['limits'][$name]['vocabulary'] = array(
      '#markup' => check_plain($limit['vocabulary']),
    );
    $form['limits'][$name]['value_field'] = array(
      '#markup' => check_plain($limit['value_field']),
    );
    $form['limits'][$name]['label_field'] = array(
      '#markup' => check_plain($limit['label_field']),
    );
    $form['limits'][$name]['edit'] = array(
      '#type' => 'link',
      '#title' => t('edit'),
      '#href' => 'admin/config/search/pz/limits/' . $name . '/edit',
    );
    $form['limits'][$name]['delete'] = array(
      '#type' => 'link',
      '#title' => t('delete'),
      '#href' => 'admin/config/search/pz/limits/' . $name . '/delete',
    );
    $form['limits'][$name]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @name', array('@name' => $name)),
      '#title_display' => 'invisible',
      '#default_value' => $limit['weight'],
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));

  return $form;
}

/**
 * Form submit handler for pz_config_limits.
 */
function pz_config_limits_submit($form, $form_state) {
  module_load_include('inc', 'pz', 'pz.limit');

  foreach ($form_state['values']['limits'] as $name => $data) {
    if (is_array($data) && isset($data['weight'])) {
      pz_limit_update(array('name' => $name, 'weight' => $data['weight']));
    }
  }
  drupal_set_message(t('The limits ordering has been saved.'));
}

/**
 * Theme for pz_config_limits.
 */
function theme_pz_config_limits($variables) {
  $form = $variables['form'];

  $header = array(
    t('Name'),
    t('Label'),
    t('Vocabulary'),
    t('Value field'),
    t('Label field'),
    t('Weight'),
    array('data' => t('Actions'), 'colspan' => 2),
  );

  $rows = array();
  foreach (element_children($form['limits']) as $name) {
    $form['limits'][$name]['weight']['#attributes']['class'] = array('limit-order-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['limits'][$name]['name']),
        drupal_render($form['limits'][$name]['label']),
        drupal_render($form['limits'][$name]['vocabulary']),
        drupal_render($form['limits'][$name]['value_field']),
        drupal_render($form['limits'][$name]['label_field']),
        drupal_render($form['limits'][$name]['weight']),
        drupal_render($form['limits'][$name]['edit']),
        drupal_render($form['limits'][$name]['delete']),
      ),
      'class' => array('draggable'),
    );
  }

  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'limit-order'),
    'empty' => t('There is no limit defined'),
  );

  $output = theme('table', $table);
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('limit-order', 'order', 'sibling', 'limit-order-weight');

  return $output;
}

/**
 * Limit configuration form.
 *
 * Used for both creation and modification.
 */
function pz_config_limit_form($form, $form_state, $name = NULL) {
  module_load_include('inc', 'pz', 'pz.limit');

  if (isset($name)) {
    $limit = pz_limit_get($name);
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of limit (should be a valid facet name)'),
    '#required' => TRUE,
    '#default_value' => isset($limit['name']) ? $limit['name'] : NULL,
    '#disabled' => isset($name) ? TRUE : FALSE,
  );

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('Displayed name'),
    '#required' => TRUE,
    '#default_value' => isset($limit['label']) ? $limit['label'] : NULL,
  );

  $form['display'] = array(
    '#type' => 'select',
    '#title' => t('Display'),
    '#description' => t('Choose the type of display'),
    '#options' => array(
      'checkboxes' => t('Checkboxes'),
      'select' => t('Dropdown list'),
      'dropdownchecklist' => t('Dropdown list with checkboxes'),
    ),
    '#default_value' => isset($limit['display']) ? $limit['display'] : 'checkboxes',
  );

  $form['select_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add "select all" element'),
    '#description' => t('Add "select all" element at the top of the list.'),
    '#default_value' => isset($limit['select_all']) ? $limit['select_all'] : 0,
    '#states' => array(
      'visible' => array(
        'select[name="display"]' => array('value' => 'dropdownchecklist'),
      ),
    ),
  );

  $form['max_items_displayed'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of items displayed at once'),
    '#default_value' => isset($limit['max_items_displayed']) ? $limit['max_items_displayed'] : 10,
    '#states' => array(
      'visible' => array(
        'select[name="display"]' => array('value' => 'dropdownchecklist'),
      ),
    ),
  );

  $vocabularies = array();
  foreach (taxonomy_get_vocabularies() as $v) {
    $vocabularies[$v->machine_name] = $v->name;
  }
  $form['vocabulary'] = array(
    '#type' => 'select',
    '#title' => 'Vocabulary',
    '#description' => 'Vocabulary used to build the list of possible choices',
    '#options' => $vocabularies,
    '#required' => TRUE,
    '#default_value' => isset($limit['vocabulary']) ? $limit['vocabulary'] : NULL,
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'pz_config_limit_add_form_ajax_callback',
      'wrapper' => 'vocabulary-config',
    ),
  );

  $form['vocabulary_config'] = array(
    '#type' => 'container',
    '#id' => 'vocabulary-config',
  );

  $vocabulary = NULL;
  if (isset($form_state['values']['vocabulary'])) {
    $vocabulary = $form_state['values']['vocabulary'];
  }
  elseif (isset($limit['vocabulary'])) {
    $vocabulary = $limit['vocabulary'];
  }

  if ($vocabulary) {
    $instances = field_info_instances('taxonomy_term', $vocabulary);
    $fields = array(
      'name' => t('Name'),
    );
    foreach ($instances as $instance) {
      $fields[$instance['field_name']] = $instance['label'];
    }
    $form['vocabulary_config']['value_field'] = array(
      '#type' => 'select',
      '#title' => t('Field for value'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => isset($limit['value_field']) ? $limit['value_field'] : NULL,
    );
    $form['vocabulary_config']['label_field'] = array(
      '#type' => 'select',
      '#title' => t('Field for label'),
      '#options' => $fields,
      '#required' => TRUE,
      '#default_value' => isset($limit['label_field']) ? $limit['label_field'] : NULL,
    );

    $v = taxonomy_vocabulary_machine_name_load($vocabulary);
    $terms = taxonomy_get_tree($v->vid);
    $terms_options = array();
    foreach ($terms as $term) {
      $label = str_repeat('-', $term->depth) . ' ' . $term->name;
      $terms_options[$term->tid] = $label;
    }
    $form['vocabulary_config']['tids'] = array(
      '#type' => 'select',
      '#title' => t('Limit terms shown'),
      '#description' => t('Show only selected terms in search form'),
      '#options' => $terms_options,
      '#multiple' => true,
      '#default_value' => isset($limit['tids']) ? unserialize($limit['tids']) : array(),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($name) ? t('Edit') : t('Add'),
  );

  return $form;
}

/**
 * Page callback for /admin/config/search/pz/limits/add.
 */
function pz_config_limit_add_form($form, $form_state) {
  return pz_config_limit_form($form, $form_state);
}

/**
 * Ajax callback to load vocabulary configuration.
 */
function pz_config_limit_add_form_ajax_callback($form) {
  return $form['vocabulary_config'];
}

/**
 * Submit callback for add form.
 */
function pz_config_limit_add_form_submit($form, $form_state) {
  module_load_include('inc', 'pz', 'pz.limit');

  $values = $form_state['values'];

  $limit = array(
    'name' => $values['name'],
    'label' => $values['label'],
    'vocabulary' => $values['vocabulary'],
    'value_field' => $values['value_field'],
    'label_field' => $values['label_field'],
    'display' => $values['display'],
    'select_all' => $values['select_all'],
    'max_items_displayed' => $values['max_items_displayed'],
    'tids' => serialize($values['tids']),
  );
  pz_limit_add($limit);

  drupal_set_message(t('New limit added'));
  drupal_goto('admin/config/search/pz/limits');
}

/**
 * Page callback for /admin/config/search/pz/limits/%/edit.
 */
function pz_config_limit_edit_form($form, $form_state, $name) {
  return pz_config_limit_form($form, $form_state, $name);
}

/**
 * Submit callback for edit form.
 */
function pz_config_limit_edit_form_submit($form, $form_state) {
  module_load_include('inc', 'pz', 'pz.limit');

  $values = $form_state['values'];

  $limit = array(
    'name' => $values['name'],
    'label' => $values['label'],
    'vocabulary' => $values['vocabulary'],
    'value_field' => $values['value_field'],
    'label_field' => $values['label_field'],
    'display' => $values['display'],
    'select_all' => $values['select_all'],
    'max_items_displayed' => $values['max_items_displayed'],
    'tids' => serialize($values['tids']),
  );
  pz_limit_update($limit);

  drupal_set_message(t('Limit updated'));
  drupal_goto('admin/config/search/pz/limits');
}

/**
 * Confirm limit deletion form.
 */
function pz_config_limit_delete_form($form, $form_state, $name) {
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );
  return confirm_form($form, t('Are you sure you want to delete this limit ?'),
    'admin/config/search/pz/limits');
}

/**
 * Submit callback for delete form.
 */
function pz_config_limit_delete_form_submit($form, $form_state) {
  module_load_include('inc', 'pz', 'pz.limit');

  $values = $form_state['values'];

  pz_limit_delete($values['name']);
  drupal_goto('admin/config/search/pz/limits');
}
