(function($) {
  $(document).ready(function() {
    $('.pz-dropdownchecklist').each(function() {
      var dropdownchecklist_options = {
        icon: {},
      };
      var $select = $(this);
      var element_to_hide;

      if ($select.is(':not(:visible)')) {
        // If select is not visible, jQuery Dropdown CheckList plugin will fail
        // to guess elements width.
        // So we show the form, apply the plugin, and hide again the form.
        element_to_hide = $select.parents(':not(:visible)').last().show();
      }

      var max_items = $select.attr('data-max-items-displayed') || 10;
      if (max_items) {
        // To calculate correct height and width, we need to apply the plugin,
        // retrieve height and width of elements, then destroy plugin, change
        // options, and re-apply plugin...

        // Calculate maxDropHeight
        max_items = (max_items > 4) ? max_items : 4;
        $select.dropdownchecklist(dropdownchecklist_options);
        var $dropcontainer = $select.siblings('.ui-dropdownchecklist-dropcontainer-wrapper')
          .children('.ui-dropdownchecklist-dropcontainer');
        var $option = $dropcontainer.children('.ui-dropdownchecklist-item').first();
        var max_height = ($option.outerHeight(true)) * max_items;
        dropdownchecklist_options.maxDropHeight = max_height;

        // Calculate width
        var max_width = 0;
        $dropcontainer.children('.ui-dropdownchecklist-item').each(function() {
          var width = 0;
          $(this).children().each(function() {
            width += $(this).outerWidth(true);
          });
          if (max_width < width) {
            max_width = width;
          }
        });

        // If first element has an empty value
        // that means this is a "select all" element
        if (!$option.find('input').val()) {
          dropdownchecklist_options.firstItemChecksAll = true;
        }

        // Add 25 more pixels to leave room for scrollbar
        dropdownchecklist_options.width = max_width + 25;

        // Destroy plugin
        $select.dropdownchecklist('destroy');
      }

      $select.dropdownchecklist(dropdownchecklist_options);

      if (element_to_hide) {
        element_to_hide.hide();
      }
    });
  });
})(jQuery);
